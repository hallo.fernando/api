<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class DataController extends Controller
{
    public function login(Request $request)
    {
        $response = "OK";
        $user_id = $request->user_id;
        $password = Hash::make($request->password);
        $name_pc = $request->name_pc;
        $timestamp = $request->timestamp;

        return response()->json(compact('response', 'user_id', 'password', 'name_pc', 'timestamp'), 200);
    }

    public function break(Request $request)
    {
        $response = "OK";
        $user_id = $request->user_id;
        $timestamp = $request->timestamp;

        return response()->json(compact('response', 'user_id','timestamp'), 200);
    }

    public function lainnya(Request $request)
    {
        $response = "OK";
        $user_id = $request->user_id;
        $reason = $request->reason;
        $timestamp = $request->timestamp;

        return response()->json(compact('response', 'user_id', 'reason','timestamp'), 200);
    }

    public function signout(Request $request)
    {
        $response = "OK";
        $user_id = $request->user_id;
        $timestamp = $request->timestamp;

        return response()->json(compact('response','user_id','timestamp'), 200);
    }
}
